# MyPodOxzone

[![CI Status](http://img.shields.io/travis/amber/MyPodOxzone.svg?style=flat)](https://travis-ci.org/amber/MyPodOxzone)
[![Version](https://img.shields.io/cocoapods/v/MyPodOxzone.svg?style=flat)](http://cocoapods.org/pods/MyPodOxzone)
[![License](https://img.shields.io/cocoapods/l/MyPodOxzone.svg?style=flat)](http://cocoapods.org/pods/MyPodOxzone)
[![Platform](https://img.shields.io/cocoapods/p/MyPodOxzone.svg?style=flat)](http://cocoapods.org/pods/MyPodOxzone)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

MyPodOxzone is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "MyPodOxzone"
```

## Author

amber, amber@inernetbs.net

## License

MyPodOxzone is available under the MIT license. See the LICENSE file for more info.
